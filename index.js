function addNum(num1, num2) {
  console.log(num1 + num2);
};

function subNum(num1, num2) {
  console.log(num1 - num2);
};
console.log("Displayed sum of 5 and 15");
addNum(5, 15);
console.log("Displayed difference of 20 and 5");
subNum(20, 5);

function multiplyNum(num1, num2) {
  return num1 * num2;
};

function divideNum(num1, num2) {
  return num1 / num2;
};


let product = multiplyNum(50, 10);
let quotient = divideNum(50, 10);

console.log("The product of 50 and 10:")
console.log(product);
console.log("The quotient of 50 and 10:")
console.log(quotient);

function getCircleArea(radius) {


  return 3.1416 * (radius ** 2)
};

let areaCircle = getCircleArea(15);

console.log("The result of getting the area of a circle with 15 radius:")
console.log(areaCircle);

function getAverage(num1, num2, num3, num4) {



  return (num1 + num2 + num3 + num4) / 4
}

let averageVar = getAverage(20, 40, 60, 80);
console.log("The average of 20,40,60 and 80: ");
console.log(averageVar);

function checkIfPassed(score, total) {

  return (score / total) * 100 > 75;

}

let isPassingScore = checkIfPassed(38, 50);
console.log("Is 38/50 a passing score?")
console.log(isPassingScore);

